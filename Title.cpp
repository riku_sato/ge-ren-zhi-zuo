#include "Title.h"
#include "Engine/GameObject/GameObject.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Title::Title(IGameObject * parent)
	: IGameObject(parent, "Title"), hPict_(-1)
{
}

//初期化
void Title::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Title.jpg");
	assert(hPict_ >= 0);
}

//更新
void Title::Update()
{
	//エンターが押されたらプレイシーンに戻る(仮)
	if (Input::IsKeyDown(DIK_RETURN))

	{
		//何らかの処理
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void Title::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Title::Release()
{
}