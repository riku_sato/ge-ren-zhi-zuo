#include "Stage.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"),hModel_(-1)
{
}

//デストラクタ
Stage::~Stage()
{
}


//初期化
void Stage::Initialize()
{
	//プレイヤーモデルロード
	hModel_ = Model::Load("data/Stage.fbx");
	assert(hModel_ >= 0);

	position_.x = -3;
	position_.z = 5;

	//BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1));
	//AddCollider(collision);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Stage::Release()
{
}