#include "Wall.h"

//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall"), hModel_(-1)
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	//プレイヤーモデルロード
	hModel_ = Model::Load("data/Wall.fbx");
	assert(hModel_ >= 0);

	position_.y = -0.3f;

	//1つ目の壁
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0.5, 3.5), D3DXVECTOR3(7.8, 1, 0.8));
	AddCollider(collision);

	//2つの目の壁
	BoxCollider* collision2 = new BoxCollider(D3DXVECTOR3(0, 0.5, -3.5), D3DXVECTOR3(7.8, 1, 0.8));
	AddCollider(collision2);
	
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Wall::Release()
{
}


