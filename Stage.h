#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/ResouceManager/Model.h"

//◆◆◆を管理するクラス
class Stage : public IGameObject
{
	int hModel_;

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};