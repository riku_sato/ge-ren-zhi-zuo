#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Player : public IGameObject
{
	int hModel_;
	D3DXVECTOR3 prePos_;	    //ポジション記憶
	D3DXVECTOR3 bulletSpeed_;	//弾の速度


public:
	//コンストラクタ
	Player(IGameObject* parent);



	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	//当たり判定
	void OnCollision(IGameObject * pTarget);

	//弾の速さのゲッター
	D3DXVECTOR3 GetBulletSpeed()
	{
		return bulletSpeed_;
	}
};

//Bullet.cpp
//Player* pPlayer = (Player*)FindObject("Player");
//D3DXVECTOR3 speed = pPlayer->GetBulletSpeed();