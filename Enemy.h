#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Enemy : public IGameObject
{

	int hModel_;
	D3DXVECTOR3 bulletSpeed_;	//弾の速度

public:
	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	//何かに当たった
    //引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
};